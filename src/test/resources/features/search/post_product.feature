Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

    @smokeTest
    Scenario Outline: user has possibility to search the available products
      Given user performs the search product of "<product>"
      Then response status code is 200

      Examples:
        | product |
        | apple   |
        | orange  |
        | pasta   |
        | cola    |

    @smokeTest
    Scenario Outline: user has possibility to search the available products
      Given user performs the search product of "<product>"
      Then the user sees empty product details response body

      Examples:
        | product |
        | apple   |
        | orange  |

  @smokeTest
  Scenario: user has no possibility to search the unavailable products
    Given user performs the search product of "car"
    Then response status code is 404
    Then response contains an error "Not found"
