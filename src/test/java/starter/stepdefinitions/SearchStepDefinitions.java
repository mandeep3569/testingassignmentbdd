package starter.stepdefinitions;

import com.starter.api.CarsAPI;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
//import net.serenitybdd.annotations.Steps;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.apache.http.HttpStatus;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.Is;
//import net.thucydides.core.annotations.Steps;
//import org.hamcrest.Matchers;

import static io.restassured.http.ContentType.JSON;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;

public class SearchStepDefinitions {

    private Response apiProductSearchResponse;

    @Steps
    public CarsAPI carsAPI;

    @When("user performs the search product of {string}")
    public void heCallsEndpoint(String arg0) {
        apiProductSearchResponse = carsAPI.searchProductByName(arg0);

    }

    @Then("response status code is {int}")
    public void verifyResponseStatusCode(int expectedStatusCode) {
        restAssuredThat(response -> apiProductSearchResponse.then().
                assertThat().statusCode(expectedStatusCode));
    }

    @Then("response contains an error {string}")
    public void verifyResponseErrorMessage(String expectedErrorMessage) {
        restAssuredThat(response -> apiProductSearchResponse.then()
                .body("detail.error", Is.is(true)));
        restAssuredThat(response -> apiProductSearchResponse.then()
                .body("detail.message", Is.is(expectedErrorMessage)));

    }

    @Then("the user sees empty product details response body")
    public void theUserSeesEmptyProductDetailsResponseBody() {
        restAssuredThat(response -> apiProductSearchResponse.then()
                .contentType(JSON)
                .statusCode(HttpStatus.SC_OK));

        MatcherAssert.assertThat("Response body was not empty!",apiProductSearchResponse.asString(),is("[]"));

    }
}
