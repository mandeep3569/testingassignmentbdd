package com.starter.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

import java.security.InvalidParameterException;
import java.text.MessageFormat;
import static io.restassured.http.ContentType.JSON;
import static java.lang.String.format;

public class CarsAPI {

    private static final EnvironmentVariables environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();

    @Step
    public Response searchProductByName(String productName) {
        return setUp()
                .basePath(format(getApiPath(), productName))
                .get();
    }

    //    @Override
    public String getApiPath() {
        return "/search/demo/%s";
    }

    public RequestSpecification setUp() {

        return SerenityRest.given()
                .baseUri(getBaseUrl())
                .contentType(JSON)
                .accept(JSON).log().ifValidationFails();
    }

    private  String getBaseUrl() {

        return environmentVariables
                .optionalProperty("products-api.baseUrl")
                .orElseThrow(() -> new InvalidParameterException(
                        MessageFormat.format("Missing property value for name {0}!", "products-api.baseUrl")));

    }
}

